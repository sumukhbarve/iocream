//
// iocream.js
//
// AUTHOR           : Sumukh Barve
//
// VERSION          : 0.6
//
// LICENSE          : the MIT License (MIT), see LICENSE.md for details
//
// BITBUCKET REPO   : https://bitbucket.org/sumukhbarve/iocream/src
//
// DOCUMENTATION    : README.md
//
// EXAMPLES & DEMOS : EXAMPLES.md
//
function IOCream(pre_id, bgcolor, outcolor, incolor, xcolor) {
    'use strict';
    var jar, jarId, jarOn, jarBuffer, that; // PRIVATE VARIABLES;
    jar = window.document.getElementById(pre_id);
    jarId = pre_id;
    jarOn = !!jar;
    jarBuffer = [];
    that = this;
    if (!jarOn) {
        window.alert('FATAL: No PRE element with the supplied `pre_id` was found.');
        return false;
    }
    if (jar.tagName.toUpperCase() !== 'PRE') {
        window.alert('FATAL: Non-PRE element with supplied `pre_id` was found.');
        return false;
    }
    //
    /////////////////////////////////////////////////
    // Styling the JAR.//////////////////////////////
    /////////////////////////////////////////////////
    //
    incolor = (!!incolor) ? incolor : 'blue';
    xcolor = (!!xcolor) ? xcolor : 'red';
    if (!jar.style.backgroundColor) { jar.style.backgroundColor = (!!bgcolor) ? bgcolor : 'white'; }
    if (!jar.style.color) { jar.style.color = (!!outcolor) ? outcolor : 'black'; }
    if (!jar.style.fontFamily) { jar.style.fontFamily = 'Monaco,Menlo,Consolas,"Courier New",monospace'; }
    //if (!jar.style.fontWeight) { jar.style.fontWeight = 'bold'; }
    //if (!jar.style.fontSize) { jar.style.fontSize = '110%'; }
    if (!jar.style.padding) { jar.style.padding = '0.75%'; }
    if (!jar.style.overflow) { jar.style.overflow = 'auto'; }
    if (!jar.style.resize) { jar.style.resize = 'both'; }
    if (!jar.style.height) { jar.style.height = '500px'; }
    if (!jar.style.border) { jar.style.border = 'thick solid silver'; }
    //
    //////////////////////////////////////////////////
    // PRIVATE FUNCTIONS./////////////////////////////
    //////////////////////////////////////////////////
    //
    function jout(mode, message, color) {
        if (!jarOn) { return false; }
        if (mode === 'buffer') {
            if (!color) {
                jarBuffer.push(message);
            } else {
                jarBuffer.push(['<span style="color:', color, ';">', message, '</span>'].join(''));
            }
        } else {
            if (!color) {
                jar.innerHTML += message;
            } else { jar.innerHTML += '<span style="color:' + color + ';">' + message + '</span>'; }
        }
        return true;
    }
    function janchor(mode, href, linktext, color) {
        if (!linktext) { linktext = href; }
        if (!color) { color = xcolor; }
        return jout(mode, ['<i><a href="', href, '" style="color:', color, ';">', linktext, '</a></i>'].join(''));
    }
    //
    /////////////////////////////////////////////////////
    // CREAM OUTPUT Methods./////////////////////////////
    /////////////////////////////////////////////////////
    //
    // `endl()` adds a NEWLINE character to CREAM OUTPUT.
    this.endl = function () { return jout('direct', '\n'); };
    //
    // `endlb()` adds a NEWLINE character to the BUFFER.
    this.endlb = function () { return jout('buffer', '\n'); };
    //
    // The following two methods output supplied messages without any manipulation.
    // `message` should be supplied for meaningful output.
    // You may OPTIONALLY supply a CSS color.
    this.jout = function (message, color) { return jout('direct', message, color); };
    this.joutb = function (message, color) { return jout('buffer', message, color); };
    //
    // The following two methods append a NEWLINE character to supplied messages.
    // `message` should be supplied for meaningful output. To simply add a NEWLINE,
    // call `endl` or `endlb` instead.
    // You may OPTIONALLY supply a CSS color.
    this.joutln = function (message, color) { return jout('direct', message + '\n', color); };
    this.joutlnb = function (message, color) { return jout('buffer', message + '\n', color); };
    //
    // The following two methods add a HORIZONTAL RULE.
    this.jhr = function () { return jout('direct', '<hr>'); };
    this.jhrb = function () { return jout('buffer', '<hr>'); };
    //
    // The following two methods aid in creating hyperlinks (by creating anchor tags.)
    // The destination url (`href`) must be supplied for meaningful output.
    // `linktext` and `color` are OPTIONAL.
    this.janchor = function (href, linktext, color) { return janchor('direct', href, linktext, color); };
    this.janchorb = function (href, linktext, color) { return janchor('buffer', href, linktext, color); };
    //
    // The following method scrolls the JAR to the end of its contents.
    // There should not really be a need to use this method, as other methods
    // incorporate scrolling when required, but it's use is not restricted.
    this.jscroll = function () {
        if (!jarOn) { return false; }
        jar.scrollTop = jar.scrollHeight;
        return true;
    };
    //
    /////////////////////////////////////////////////////
    // FULSHING THE BUFFER.//////////////////////////////
    /////////////////////////////////////////////////////
    //
    this.jflushb = function (flush) {
        if (!jarOn) { return false; }
        that.jout(jarBuffer.join(''));
        jarBuffer = [];
        if (flush !== false) { that.jscroll(); } // To perform a flush-less scroll, explicitly set flush = false.
        return true;
    };
    //
    /////////////////////////////////////////////////////
    // KEYBOARD INPUT Methods.///////////////////////////
    /////////////////////////////////////////////////////
    //
    // These are NON-blocking functions.
    // A callback function must be supplied.
    //
    // `jin` produces an inline text-field for user input.
    this.jin = function (callback, message, suggestion, color) {
        var fobj, lobj, sobj, iobj; // form, label, span and input elements respectively.
        if (!jarOn || typeof callback !== 'function') { return false; }
        if (message === undefined) { message = ''; }
        if (suggestion === undefined) { suggestion = ''; }
        if (!color) { color = incolor; }
        // FORM:
        fobj = document.createElement('form');
        fobj.id = '_jin_form_' + jarId;
        fobj.style.display = 'table';
        fobj.style.width = '100%';
        // LABEL:
        lobj = document.createElement('label');
        lobj.style.display = 'table-cell';
        lobj.style.color = color;
        // SPAN:
        sobj = document.createElement('span');
        sobj.style.display = 'table-cell';
        sobj.style.width = '100%';
        // INPUT:
        iobj = document.createElement('input');
        iobj.id = '_jin_input_' + jarId;
        iobj.style.width = '100%';
        iobj.style.fontFamily = 'monospace';
        iobj.style.fontWeight = 'bold';
        iobj.value = suggestion;
        // FORM ID and ONSUBMIT:
        fobj.onsubmit = function () {
            var iVal, newspan, oldform;
            iVal = document.getElementById('_jin_input_' + jarId).value;
            newspan = document.createElement('span');
            newspan.style.color = color;
            newspan.appendChild(document.createTextNode(message + iVal + '\n'));
            oldform = document.getElementById('_jin_form_' + jarId);
            jar.replaceChild(newspan, oldform);
            callback(iVal);
            return false;                   // So that the form does NOT submit.
        };
        // APPENDING CHILDREN
        lobj.appendChild(document.createTextNode(message));
        sobj.appendChild(iobj);
        fobj.appendChild(lobj);
        fobj.appendChild(sobj);
        jar.appendChild(fobj);
        iobj.focus();
        iobj.select();
        return true;
    };
    //
    // `jinarea` produces an block textarea for input, and a "Submit" button.
    this.jinarea = function (callback, message, suggestion, buttonText, color) {
        var fobj, tobj, bobj; // FORM, TEXTAREA and BUTTON elements
        if (!color) { color = incolor; }
        if (!that.jout(message, color) || typeof callback !== 'function') { return false; }
        if (!buttonText) { buttonText = 'Submit'; }
        // TEXTAREA:
        tobj = document.createElement('textarea');
        tobj.id = '_jinarea_textarea_' + jarId;
        tobj.style.width = '100%';
        tobj.style.display = 'block';
        tobj.value = (!!suggestion) ? suggestion : '';
        // FORM:
        fobj = document.createElement('form');
        fobj.id = '_jinarea_form_' + jarId;
        fobj.onsubmit = function () {
            var tVal, oldform;
            tVal = document.getElementById('_jinarea_textarea_' + jarId).value;
            oldform = document.getElementById('_jinarea_form_' + jarId);
            jar.removeChild(oldform);
            that.joutln(tVal, color);
            callback(tVal);
            return false;
        };
        // SUBMIT BUTTON:
        bobj = document.createElement('button');
        bobj.appendChild(document.createTextNode(buttonText));
        // APPENDING CHILDREN:
        fobj.appendChild(tobj);
        fobj.appendChild(bobj);
        jar.appendChild(fobj);
        tobj.focus();
        tobj.select();
        return true;
    };
    //
    /////////////////////////////////////////////////////
    // MOUSE INPUT Method.///////////////////////////////
    /////////////////////////////////////////////////////
    //
    // `jclick` is a non-blocking function.
    // A callback function must be supplied.
    this.jclick = function (callback, buttonText1) {
        var dobj, bobjs = ['empty'], i; // DIV, BUTTONs elements respectively.
        if (!jarOn || typeof callback !== 'function') { return false; }
        if (buttonText1 === undefined) { buttonText1 = 'Go'; }
        dobj = document.createElement('div');
        dobj.style.display = 'inline';
        dobj.id = '_jclick_div_' + jarId;
        for (i = 1; i < arguments.length; i += 1) {
            bobjs[i] = document.createElement('button');
            bobjs[i].appendChild(document.createTextNode(arguments[i]));
            dobj.appendChild(bobjs[i]);
            //if (i === 1) { bobjs[i].autofocus = true; }
            bobjs[i].onclick = (function (current_i) {
                return function () {
                    var bTxt, olddiv, newbutton;
                    bTxt = bobjs[current_i].innerHTML;
                    newbutton = document.createElement('button');
                    newbutton.appendChild(document.createTextNode(bTxt));
                    newbutton.disabled = true;
                    olddiv = document.getElementById('_jclick_div_' + jarId);
                    jar.replaceChild(newbutton, olddiv);
                    jar.appendChild(document.createTextNode('\n'));
                    callback(bTxt);
                    return true;
                };
            }(i));
            if (i === 1) { bobjs[i].autofocus = true; }
            dobj.appendChild(bobjs[i]);
        }
        jar.appendChild(dobj);
        return true;
    };
    //
    //////////////////////////////////////////////////////
    // WRAPPERS AROUND INBUILT Javascript FUNCTIONS.//////
    //////////////////////////////////////////////////////
    //
    this.jprompt = function (message, suggestion, color) {
        if (!jarOn) { return false; }
        if (!color) { color = xcolor; }
        var inp = window.prompt(message, suggestion);
        that.joutln(message + inp, color);
        return inp;
    };
    this.jconfirm = function (message, color) {
        if (!jarOn) { return null; }
        if (!color) { color = xcolor; }
        var bull = window.confirm(message), buttonName;
        buttonName = (!!bull) ? 'OK' : 'Cancel';
        that.joutln(message + buttonName, color);
        return bull;
    };
    this.jalert = function (message, color) {
        if (!jarOn) { return false; }
        if (!color) { color = xcolor; }
        window.alert(message);
        that.joutln(message, color);
        return true;
    };
    //
    ///////////////////////////////////////////////////////
    // End of execution.///////////////////////////////////
    ///////////////////////////////////////////////////////
    //
    this.jclose = function (buttonText, scroll) {
        if (!jarOn) { return false; }
        if (!buttonText) { buttonText = 'Reload'; } // `buttonText` is optional. 
        that.jout('<hr>');
        that.joutln(['<button onclick="window.location.reload();">', buttonText, '</button>'].join(''));
        if (scroll !== false) { that.jscroll(); }   // To avoid scrolling, explicitly set scroll to false.
        jar = null;
        jarOn = false;
        return true;
    };
    //
    ////////////////////////////////////////////////////////
    // A window into private properties.////////////////////
    ////////////////////////////////////////////////////////
    //
    this.jstatus = function () {
        var arr = [];
        arr.push('********************************');
        arr.push('\tSTATUS REPORT:-');
        if (jarOn) {
            arr.push('Jar Binding: Active');
        } else {
            arr.push('Jar Binding: Inactive');
        }
        arr.push('Jar ID: ' + jarId);
        if (jarBuffer.length === 0) {
            arr.push('Jar Buffer: Empty');
        } else {
            arr.push('Jar Buffer: Non-empty');
            arr.push('Buffer-length: ' + jarBuffer.length);
        }
        arr.push('********************************');
        window.alert(arr.join('\n'));
    };
}
