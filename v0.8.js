// iocream.js - v0.8
// -------------------
// UNDER DEVELOPMENT |
// -------------------
// Author : Sumukh Barve
// License: see LICENSE.md
//
var IOCream = function (preid, styleobj) {
    'use strict';
    var prenode, that, fragment, addnode, print, endl, println, printhr,
        printlink, stylize, unprint, input, temp_i; //unprintall
    prenode = document.getElementById(preid);
    if (!prenode) {
        throw new Error('\tiocream.js says:\n\n\tHTML element with supplied ID, "' + preid + '", was not found.');
    } else if (prenode.nodeName.toUpperCase() !== 'PRE') {
        throw new Error('\tiocream.js says:\n\n\tHTML ' + prenode.nodeName.toUpperCase() +
            ' with supplied ID, "' + preid + '", was found. PRE element was expected');
    }
    // otherwise...
    if (!styleobj) {
        styleobj = {color : 'white',
                    backgroundColor : 'black',
                    padding : '0.75%',
                    margin : '0.75%',
                    fontFamily : 'Monaco,Menlo,Consolas,"Courier New",monospace',
                    overflow : 'auto',
                    resize : 'both',
                    //width : '100%',
                    height : '450px'
                   };
    }
    for (temp_i in styleobj) {
        if (styleobj.hasOwnProperty(temp_i) && typeof styleobj[temp_i] === 'string') {
            prenode.style[temp_i] = styleobj[temp_i];
        }
    }
    that = this;
    fragment = document.createDocumentFragment();
    this.buffer = {};
    //
    // PRIVATE FUNCTIONS...
    addnode = function (newnode, mode) {
        if (mode === 'buffer') {
            fragment.appendChild(newnode);
        } else {
            prenode.appendChild(newnode);
        }
        return that;
    };
    print = function (msg, mode) {
        var spannode;
        if (msg === undefined) { return that; }
        spannode = document.createElement('SPAN');
        spannode.appendChild(document.createTextNode(msg));
        return addnode(spannode, mode);             // returns that
    };
    println = function (msg, mode) {                // returns that
        return (msg === undefined) ? print('\n', mode) : print(msg + '\n', mode);
    };
    endl = function (mode) {
        // Unlike println, endl, except in the below edge case,
        // does NOT create a new node. Thereby, stylize can be
        // (mostly) meaningfully called on it.
        var parent = (mode === 'buffer') ? fragment : prenode;
        if (parent.childElementCount === 0) {       // edge case
            return print('\n', mode);               // returns that
        }
        // otherwise...
        parent.lastChild.appendChild(document.createTextNode('\n'));
        return that;
    };
    printhr = function (mode) {
        var hrnode = document.createElement('HR');
        return addnode(hrnode, mode);               // returns that
    };
    printlink = function (href, linktext, mode) {
        var anode;
        anode = document.createElement('A');
        anode.href = href;
        anode.appendChild(document.createTextNode(linktext || href));
        return addnode(anode, mode);                // returns that
    };
    stylize = function (styleobj, mode) {
        var i, parent = (mode === 'buffer') ? fragment : prenode;
        for (i in styleobj) {
            if (styleobj.hasOwnProperty(i) && typeof styleobj[i] === 'string') {
                parent.lastChild.style[i] = styleobj[i];
            }
        }
        return that;
    };
    unprint = function (mode) {
        var parent, lastchild;
        parent = (mode === 'buffer') ? fragment : prenode;
        lastchild = parent.lastChild;
        if (lastchild) { parent.removeChild(lastchild); }
        return that;
    };
    //
    // PUBLIC FUNCTIONS:
    this.print = function (msg) { return print(msg); };
    this.buffer.print = function (msg) { return print(msg, 'buffer'); };
    this.endl = function () { return endl(); };
    this.buffer.endl = function () { return endl('buffer'); };
    this.println = function (msg) { return println(msg); };
    this.buffer.println = function (msg) { return println(msg, 'buffer'); };
    this.printhr = function () { return printhr(); };
    this.buffer.printhr = function () { return printhr('buffer'); };
    this.printlink = function (href, linktext) { return printlink(href, linktext); };
    this.buffer.printlink = function (href, linktext) { return printlink(href, linktext, 'buffer'); };
    this.stylize = function (styleobj) { return stylize(styleobj); };
    this.buffer.stylize = function (stylize) { return stylize(styleobj, 'buffer'); };
    this.unprint = function () { return unprint(); };
    this.buffer.unprint = function () { return unprint('buffer'); };
    //
    this.scroll = function () {
        // Scrolls to latest output.
        prenode.scrollTop = prenode.scrollHeight;
        return that;
    };
    this.buffer.reset = function () {
        fragment = document.createDocumentFragment();
        return that;
    };
    this.buffer.flush = function (scroll) {
        // Scrolling can be turned-off by setting scroll to `false`
        prenode.appendChild(fragment);
        if (scroll !== false) { that.scroll(); }
        return that.buffer.reset();
    };
    this.buffer.size = function () {
        return fragment.childElementCount;
    };
    //
    this.idle = function (callback, timeout) {
        setTimeout(callback, timeout);
        return;
    };
    //
    // PRIVATE (KEYBOARD) INPUT FUNCTION:
    input = function(callback, default_text, style_obj, mode) {
        var input_node, input_value, button_node, i;
        if (typeof callback !== 'function') {
            throw new TypeError('iocream.js says:\n\n\tType of supplied `callback` argument is '
                + typeof callback +'. Expected type was function.');
        }
        style_obj = (style_obj !== undefined) ? style_obj : {
                width : (mode === 'TEXTAREA') ? '90%' : '',
                fontFamily: 'Monaco,Menlo,Consolas,"Courier New",monospace'
            };
        input_node = document.createElement(mode);
        input_node.value = (default_text !== undefined) ? default_text : '';
        for (i in style_obj) {
            if (style_obj.hasOwnProperty(i) && typeof style_obj[i] === 'string') {
                input_node.style[i] = style_obj[i];
            }
        }
        button_node = document.createElement('BUTTON');
        button_node.appendChild(document.createTextNode('Submit'));
        button_node.onclick = function () {
            input_value = input_node.value;   // get-input value
            input_node.disabled = true;      // disable field and button
            button_node.disabled = true;
            that.println();
            callback(input_value);
            return;
        }
        if (mode === 'TEXTAREA') { that.println(); }
        prenode.appendChild(input_node);
        if (mode === 'TEXTAREA') {that.println(); }
        prenode.appendChild(button_node);
        input_node.focus();
        if (default_text !== undefined) { input_node.select(); }
    };
    //
    // PUBLIC (KEYBOARD) INPUT FUNCTIONS:
    this.input = function (callback, default_text, style_obj) {
        return input(callback, default_text, style_obj, 'INPUT'); // returns undefined
    };
    this.inputarea = function (callback, default_text, style_obj) {
        return input(callback, default_text, style_obj, 'TEXTAREA'); // returns undefined
    };
};
